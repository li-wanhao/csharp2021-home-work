﻿using ImportData.Data;
using ImportData.Services;
using System.Collections.Generic;

namespace ImportData
{
    /// <summary>
    /// 求data.CSV中学生成绩平均分、最高分、最低分
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //定义学生集合students
            List<Student> students = new List<Student>();
            //将data.CSV班级信息读取进入students集合中
            students = StudentServices.ReadFromCSV();
            //输出班级最高分、最低分成绩
            StudentServices.OutputImportData(students);
        }
    }
}
