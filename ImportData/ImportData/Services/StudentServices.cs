﻿using ImportData.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ImportData.Services
{
    class StudentServices
    {
        /// <summary>
        /// 从data.CSV中读取学生信息，并返回students集合
        /// </summary>
        /// <returns></returns>
        public static List<Student> ReadFromCSV()
        {
            //定义学生集合students
            var students = new List<Student>();
            //文件的相对路径
            string path = @"D:\c\homework2021\ImportData\data.CSV";
            StreamReader streamReader = new StreamReader(path);
            //读取该文件的行信息
            string Line = streamReader.ReadLine();
            Line = streamReader.ReadLine();
            //将文件中每一行的信息赋值给Student类中的对象
            while (Line != null)
            {
                //构造函数，将Line作为参数传递给函数Student
                students.Add(new Student(Line));
                //继续读取该文件的下一行
                Line = streamReader.ReadLine();
            }
            return students;
        }
        /// <summary>
        /// 求班级同学平均分、最高分、最低分
        /// </summary>
        /// <param name="students"></param>
        public static void OutputImportData(List<Student> students)
        {
            //判断students集合是否为空
            if (!students.Any())
            {
                Console.WriteLine("There is no record.");
                return;
            }
            //求班级同学的平均分
            var average = students.Average(a => a.Grade);
            Console.WriteLine($"班级同学的平均分为：{average}");
            //求最高分同学成绩
            var max = students.Max(a => a.Grade);
            foreach (var item in students)
            {
                if (item.Grade == max)
                {
                    Console.WriteLine($"{item.Name}同学取得最高分：{max}");
                }
            }
            //求最低分同学成绩
            var min = students.Min(a => a.Grade);
            foreach (var item in students)
            {
                if (item.Grade == min)
                {
                    Console.WriteLine($"{item.Name}同学取得最低分：{min}");
                }
            }
        }
    }
}
