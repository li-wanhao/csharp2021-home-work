﻿using ImportDataThree.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace ImportDataThree.Services
{
    class StudentServices1
    {
        /// <summary>
        /// 从data.CSV中读取学生信息，并返回students1集合
        /// </summary>
        /// <returns></returns>
        public static List<Student1> ReadFromCSV()
        {
            //定义学生集合students1
            var students1 = new List<Student1>();
            //文件的相对路径
            string path = @"D:\c\homework2021\ImportDataThree\data.CSV";
            StreamReader streamReader = new StreamReader(path);
            //读取该文件的行信息
            string Line = streamReader.ReadLine();
            Line = streamReader.ReadLine();
            //将文件中每一行的信息赋值给Student1类中的对象
            while (Line != null)
            {
                //构造函数，将Line作为参数传递给函数Student1
                students1.Add(new Student1(Line));
                //继续读取该文件的下一行
                Line = streamReader.ReadLine();
            }
            return students1;
        }
        /// <summary>
        /// 求班级同学平均分、最高分、最低分
        /// </summary>
        /// <param name="students1"></param>
        public static void OutputImportData(List<Student1> students1)
        {
            //判断students1集合是否为空
            if (!students1.Any())
            {
                Console.WriteLine("There is no record.");
                return;
            }
            //求班级同学的平均分
            var average = students1.Average(a => a.Grade);
            Console.WriteLine($"班级同学的平均分为：{average}");
            //求最高分同学成绩
            var max = students1.Max(a => a.Grade);
            foreach (var item in students1)
            {
                if (item.Grade == max)
                {
                    Console.WriteLine($"{item.Name}同学取得最高分：{max}");
                }
            }
            //求最低分同学成绩
            var min = students1.Min(a => a.Grade);
            foreach (var item in students1)
            {
                if (item.Grade == min)
                {
                    Console.WriteLine($"{item.Name}同学取得最低分：{min}");
                }
            }
        }
    }
}