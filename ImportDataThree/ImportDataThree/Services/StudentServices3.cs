﻿using ImportDataThree.Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImportDataThree.Services
{
    public class StudentServices3
    {
        //定义委托
        public delegate void Mydeledate(string message);
        //定义事件
        public event Mydeledate MyMessage;
        /// <summary>
        /// 从数据库中返回Student3表中所有记录
        /// </summary>
        public static List<Student3> ReadAllFromDataBase()
        {
            var db = new DBEntities();
            var students3 = db.Student3;
            return students3.ToList();
        }
        /// <summary>
        /// 输出结果
        /// </summary>
        /// <param name="students3"></param>
        public void Output(List<Student3> students3)
        {
            foreach (var item in students3)
            {
                //调用事件
                MyMessage($"{item.Name} {item.ID} {item.Gender} {item.BirthdayTime} {item.Phone} {item.Grade}");
            }
        }
        /// 求班级同学平均分、最高分、最低分
        /// </summary>
        /// <param name="students"></param>
        public static void OutputImportData(List<Student3> students3)
        {
            //判断students3集合是否为空
            if (!students3.Any())
            {
                Console.WriteLine("There is no record.");
                return;
            }
            //求班级同学的平均分
            var average = students3.Average(a => a.Grade);
            Console.WriteLine($"班级同学的平均分为：{average}");
            //求最高分同学成绩
            var max = students3.Max(a => a.Grade);
            foreach (var item in students3)
            {
                if (item.Grade == max)
                {
                    Console.WriteLine($"{item.Name}同学取得最高分：{max}");
                }
            }
            //求最低分同学成绩
            var min = students3.Min(a => a.Grade);
            foreach (var item in students3)
            {
                if (item.Grade == min)
                {
                    Console.WriteLine($"{item.Name}同学取得最低分：{min}");
                }
            }
        }
    }
}