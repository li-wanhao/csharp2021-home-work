﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportDataThree.Data
{
    class Student2
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime BirthdayTime { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 成绩
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="Line"></param>
        public Student2(string name, string id, string gender, DateTime birthdayTime, string phone, int grade)
        {
            //将data中的数据赋值Student类中的对象
            Name = name;
            ID = id;
            Gender = gender;
            BirthdayTime = birthdayTime;
            Phone = phone;
            Grade = grade;
        }
    }
}
