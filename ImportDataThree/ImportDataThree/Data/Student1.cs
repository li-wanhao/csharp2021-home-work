﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ImportDataThree.Data

{
    class Student1
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime BirthdayTime { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 成绩
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="Line"></param>
        public Student1(string Line)
        {
            //定义一个数组，将原Line数组中每行信息按照逗号分隔
            string[] datas = Line.Split(',');
            //将datas中的数据赋值Student类中对象
            Name = datas[0];
            ID = datas[1];
            Gender = datas[2];
            BirthdayTime = Convert.ToDateTime(datas[3]);
            Phone = datas[4];
            Grade = Convert.ToInt32(datas[5]);
        }
    }
}