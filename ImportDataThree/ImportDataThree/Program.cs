﻿using ImportDataThree.Data;
using ImportDataThree.Models.EF;
using ImportDataThree.Services;
using System;
using System.Collections.Generic;

namespace ImportData3
{
    /// <summary>
    /// 使用三种方法获取班级学生成绩平均分、最高分、最低分
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("直接导入CSV结果为：");
            //定义学生集合students1
            List<Student1> students1 = new List<Student1>();
            //将data.CSV班级信息读取进入students1集合中
            students1 = StudentServices1.ReadFromCSV();
            //输出班级最高分、最低分成绩
            StudentServices1.OutputImportData(students1);
            Console.WriteLine(" ");

            Console.WriteLine("使用NPOI导入Excel结果为：");
            //定义学生集合students2
            List<Student2> students2 = new List<Student2>();
            //将data.CSV班级信息读取进入students2集合中
            students2 = StudentServices2.ReadFromExcel();
            //输出班级最高分、最低分成绩
            StudentServices2.OutputImportData(students2);
            Console.WriteLine("");

            Console.WriteLine("从数据库导入数据结果为：");
            var service = new StudentServices3();
            //当MyMessage事件发生时，执行Service_MyMessage
            service.MyMessage += Service_MyMessage;
            List<Student3> students3 = new List<Student3>();
            students3 = (List<Student3>)StudentServices3.ReadAllFromDataBase();
            service.Output(students3);
            Console.WriteLine("");
            StudentServices3.OutputImportData(students3);
            Console.ReadKey();
        }
        /// <summary>
        /// 事件发生时执行
        /// </summary>
        /// <param name="message"></param>
        private static void Service_MyMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}