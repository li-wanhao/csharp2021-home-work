﻿using System;

namespace Fermat
{
    /// <summary>
    /// 验证费马大定理，条件n=3,x,y,z<10000
    /// </summary>
    class Program
    {
        static int Max = 10000;//定义一个公用变量Max
        static long[] Numbers = new long[Max];//定义一个公用数组Numbers
        static void Main(string[] args)
        {
            var startTime = System.DateTime.Now;//开始时间
            for (long i = 0; i < Max; i++)
            {
                Numbers[i] = (i + 1) * (i + 1) * (i + 1);//给Numebers数组依次赋值
            }
            var Flag = true;
            for (int i = 0; i < Max - 1 && Flag; i++)
            {
#if DEBUG 
                Console.SetCursorPosition(5, 5);//设置光标的位置
                Console.WriteLine($"i={i}");
                var midTime = System.DateTime.Now;
                var tsmid = midTime - startTime;
                Console.SetCursorPosition(5, 6);
                Console.WriteLine($"共{tsmid.TotalSeconds}秒");//实时输出调试到当前位置的时间
#endif 
                for (int j = i; j < Max - 1 && Flag; j++)
                {
                    var sum = Numbers[i] + Numbers[j];//将x*x*x+y*y*y的值赋给sum
                    if (Search(sum))//调用函数，如果在数组中找到一个值等于sum，则进行下面的语句
                    {
                        Flag = false;
                        Console.WriteLine($"When x={i + 1},y={j + 1},the Theory of Fermat is wrong.");
                    }
                }
            }
            if (Flag)//如果没有在数组中找到一个值等于sum，则进行下面的语句
            {
                Console.WriteLine("The Theory of Fermat is right.");
            }
            var endTime = System.DateTime.Now;//结束时间
            var ts = endTime - startTime;
            Console.WriteLine($"共{ts.TotalSeconds}秒");//输出从调试开始到结束需要的时间
        }
        /// <summary>
        /// 调用一个函数，通过该函数来查询在数组Numbers中是否存在一个值等于sum
        /// 该方法为二分法
        /// </summary>
        /// <param name="array"></param>
        static bool Search(long key)
        {
            int left = 0;
            int right = Max - 1;
            int mid;
            while (left <= right)//判断该范围的左区间号是否小于等于右区间号
            {
                mid = (left + right) / 2;
                if (Numbers[mid] < key)
                {
                    left = mid + 1;//如果mid所对应的数组值小于所要找的值，则将mid+1赋值给左区间号
                }
                else if (Numbers[mid] > key)
                {
                    right = mid - 1;//如果mid所对应的数组值大于所要找的值，则将mid-1赋值给右区间号
                }
                else
                {
                    return true;//如果mid所对应的数组值等于所要找的值，则在数组Numbers中存在一个值等于sum，返回true，但是此时表示费马大定理不成立
                }
            }
            return false;//如果在数组Numbers中不存在一个值等于sum，返回false，但是此时表示费马大定理成立
        }
    }
}
