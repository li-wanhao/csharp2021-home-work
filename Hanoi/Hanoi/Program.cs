﻿using System;

namespace Hanoi
{
    class Program
    {
        /// <summary>
        /// 编写汉诺塔程序，条件为n=7
        /// </summary>
        /// <param name="args"></param>
        static int n = 7;
        static int Sum = 0;
        static void Main(string[] args)
        {
            Sum = Move(n, 'A', 'B', 'C');//将A上面的n个盘借助B移动到C上需要的步数
            Console.WriteLine($"一共需要移动{Sum}步");
            Console.ReadKey();
        }
        static int Move(int n,char A,char B,char C)
        {
            if (n==1)
            {
                Console.WriteLine($"第{n}号盘从{A}—>{C}");//当n等于1时，该盘直接从A到C；
                Sum++;
            }
            else
            {
                Move(n - 1, A, C, B);//当n>1时，先将n-1个盘从A借助C移到B；
                Console.WriteLine($"第{n}号盘从{A}—>{C}");//输出n-1个盘从A借助C移到B的步骤
                Sum++;//步数累加
                Move(n - 1, B, A, C);//再将n-1个盘从B借助A移到C
            }
            return Sum;
        }
    }
}
