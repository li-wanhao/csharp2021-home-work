﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyClock
{
    public partial class UcClock : UserControl
    {
        public UcClock()
        {
            InitializeComponent();
        }

        private void UcClock_Load(object sender, EventArgs e)
        {

        }
        //绘制或重新绘制时发生
        private void UcClock_Paint(object sender, PaintEventArgs e)
        {
            //定义背景色
            var g = e.Graphics;
            g.Clear(Color.DarkRed);

            //定义表框
            float width = (float)2;
            var pen = new Pen(new SolidBrush(Color.Gold), width);
            var rec = new RectangleF(10, 10, 300, 300);
            g.DrawEllipse(pen, rec);

            //以文本方式输出当前时间
            var font = new Font("华文中宋", 15);
            var brush = new SolidBrush(Color.DarkOrange);
            var point = new Point(60, 90);
            g.DrawString(System.DateTime.Now.ToString(), font, brush, point);
            
            //坐标系转移和旋转
            var center = new Point(this.Width / 7 * 2, this.Height / 2);
            g.TranslateTransform(center.X, center.Y);

            //定义刻度盘
            var point1 = new Point(-15,-150);
            var point2 = new Point(120, -13);
            var point3 = new Point(-15, 110);
            var point4 = new Point(-140, -13);
            g.DrawString("12", font, brush, point1);
            g.DrawString("3", font, brush, point2);
            g.DrawString("6", font, brush, point3);
            g.DrawString("9", font, brush, point4);

            //秒
            var seconds = System.DateTime.Now.Second;
            g.RotateTransform(seconds * 360 / 60);
            var pen1 = new Pen(new SolidBrush(Color.Red));
            var p1 = new Point(0, 0);
            var p2 = new Point(0, -130);
            g.DrawLine(pen1, p1, p2);
            g.RotateTransform(seconds * 360 / 60 * (-1));

            //分
            var minutes = System.DateTime.Now.Minute;
            g.RotateTransform((seconds + minutes * 60) * 360 / 60 / 60);
            var pen2 = new Pen(new SolidBrush(Color.Black));
            var p3 = new Point(0, 0);
            var p4 = new Point(0, -100);
            g.DrawLine(pen2, p3, p4);
            g.RotateTransform((seconds + minutes * 60) * 360 / 60 / 60 * (-1));

            //时
            var hours = System.DateTime.Now.Hour;
            g.RotateTransform((seconds + minutes * 60 + (hours - 12) * 3600) * 360 / 3600 / 12);
            float wid = (float)3;
            var pen3 = new Pen(new SolidBrush(Color.Black), wid);
            var p5 = new Point(0, 0);
            var p6 = new Point(0, -80);
            g.DrawLine(pen3, p5, p6);
            g.RotateTransform((seconds + minutes * 60 + (hours - 12) * 3600) * 360 / 3600 / 12 * (-1));

        }

        //定时重绘控件
        private void Timer1_Tick(object sender, EventArgs e)
        {
            this.Invalidate();
        }
    }
}
