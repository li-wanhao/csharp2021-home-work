﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyWindows
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello World!");
            this.buttonOK.Text = "北京时间";
        }

        private void frmMain_Load_1(object sender, EventArgs e)
        {
            this.labelMessage.Text = System.DateTime.Now.ToString();
        }

        private void Mytimer_Tick(object sender, EventArgs e)
        {
            this.labelMessage.Text = System.DateTime.Now.ToString();
        }
    }
}
