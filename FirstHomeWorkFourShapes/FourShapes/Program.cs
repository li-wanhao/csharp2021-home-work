﻿using System;

namespace FourShapes
{
    /// <summary>
    /// 输出四种形状三角形
    /// </summary>
    class Program
    {
        private const int LinesConst = 11;
        static void Main(string[] args)
        {
            Console.Clear();
            RightAngledTriangle();
            Console.ReadKey();
            Console.Clear();
            IsoscelesTriangle();
            Console.ReadKey();
            Console.Clear();
            HollowIsoscelesTriangle();
            Console.ReadKey();
            Console.Clear();
            HollowDiamond();
            Console.ReadKey();
        }
        ///输出直角三角形
        static void RightAngledTriangle()
        {
            Console.WriteLine("输出直角三角形");
            for (int i = 0; i < LinesConst; i++)
            {
                Console.WriteLine(new String('*', i + 1));//建立长度为（i+1），内容为*的字符串
            }
        }
        ///输出等腰三角形
        static void IsoscelesTriangle()
        {
            Console.WriteLine("输出等腰三角形");
            for (int i = 1; i <= LinesConst; i++)
            {
                Console.Write(new String(' ', LinesConst - i));//建立长度为（LinesConst-i），内容为 的字符串
                Console.Write(new String('*', 2 * i - 1));//建立长度为（2*i-1），内容为*的字符串
                Console.WriteLine();
            }
        }
        ///输出空心等腰三角形
        static void HollowIsoscelesTriangle()
        {
            Console.WriteLine("输出空心等腰三角形");
            for (int i = 1; i <= LinesConst; i++)
            {
                if (i == 1)
                {
                    Console.Write(new String(' ', LinesConst - i));//建立长度为（LinesConst - i），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.WriteLine();
                }
                else if(i > 1 && i < LinesConst)
                {
                    Console.Write(new String(' ', LinesConst - i));//建立长度为（LinesConst-i），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.Write(new String(' ', 2 * i - 3));//建立长度为（2*i-3），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine(new String('*', 2 * i - 1));//建立长度为（2*i-1），内容为*的字符串
                }
            }
        }
        /// <summary>
        /// 输出空心菱形
        /// </summary>
        static void HollowDiamond()
        {
            Console.WriteLine("输出空心菱形");
            for (int i = 1; i <= LinesConst; i++)
            {
                if (i == 1)
                {
                    Console.Write(new String(' ', LinesConst - i));//建立长度为（LinesConst-i），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.WriteLine();
                }
                else
                {
                    Console.Write(new String(' ', LinesConst - i));//建立长度为（LinesConst-i），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.Write(new String(' ', 2 * i - 3));//建立长度为（2*i-3），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.WriteLine();
                }
            }
            for (int i = 1; i <= LinesConst - 1; i++)
            {
                if (i >= 1 && i < LinesConst - 1)
                {
                    Console.Write(new String(' ', i));//建立长度为i，内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.Write(new String(' ', 19 - 2 * i));//建立长度为（19-2*i），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.WriteLine();
                }
                else
                {
                    Console.Write(new String(' ', LinesConst - 1));//建立长度为（LinesConst-1），内容为 的字符串
                    Console.Write(new String('*', 1));//建立长度为1，内容为*的字符串
                    Console.WriteLine();
                }
            }
        }
    }
}
