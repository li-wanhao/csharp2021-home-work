﻿using System;

namespace Array
{
    class Program
    {
        /// <summary>
        /// 创建一个调用函数，验证当改变形参中数组的值，实参中是否发生改变
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            int[] Array = new int[] { 2, 4, 6, 8, 10 };//定义一个数组
            Console.Write("调用函数前的数组为：");
            for (int i = 0; i <Array.Length; i++)
            {
                Console.Write(Array[i]+" ");//输出调用函数前该数组的值
            }
            Console.WriteLine();
            Func(Array);//调用函数
            Console.Write("调用函数后的数组为：");
            for (int i = 0; i < Array.Length; i++)
            {
                Console.Write(Array[i]+" ");//输出调用函数后数组的值
            }
            Console.WriteLine();
        }
        static void Func(int[] Array)
        {
            for (int i = 0; i < Array.Length; i++)
            {
                Array[i] = Array[i] - 1;//改变调用函数中数组的值
            }
            Console.Write("改变后函数中的数组为：");
            for (int i = 0; i < Array.Length; i++)
            {
                Console.Write(Array[i]+" ");//输出改变后函数中的数组的值
            }
            Console.WriteLine();
        }
    }
}
