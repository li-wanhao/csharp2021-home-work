﻿using ImportExcel.Data;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ImportData.Services
{
    class StudentServices
    {
        private static readonly string sheetName;
        /// <summary>
        /// 从Data.xlsx中读取学生信息，并返回students集合
        /// </summary>
        /// <returns></returns>
        public static List<Student> ReadFromExcel()
        {
            //定义学生集合students
            var students = new List<Student>();
            //文件的相对路径
            string path = @"D:\c\homework2021\ImportExcel\Data.xls";
            FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            //创建数据结构
            IWorkbook workbook = WorkbookFactory.Create(fileStream);
            //创建表
            ISheet sheet = null;
            //判断所要查找的表是否为空或者不存在
            if (!string.IsNullOrEmpty(sheetName))
            {
                sheet = workbook.GetSheet(sheetName);
                if (sheet == null)
                {
                    sheet = workbook.GetSheetAt(0);
                }
            }
            else
            {
                sheet = workbook.GetSheetAt(0);
            }
            //获取表的第一行
            IRow firstrow = sheet.GetRow(0);
            //定义一个变量，将每行的单元格数量赋值给该变量
            int Columncount = firstrow.LastCellNum;
            //定义一个变量，将该表的行数赋值给该变量
            int Rowcount = sheet.LastRowNum;
            //定义一个数组，依次获取该表格单元格中的内容
            string[] data = new string[Columncount];
            for (int i = 1; i <= Rowcount; i++)
            {
                for (int j = 0; j < Columncount; j++)
                {
                    data[j] = sheet.GetRow(i).GetCell(j).ToString();
                }
                //构造一个函数，将data作为参数进行传递
                students.Add(new Student(name:data[0],
                id: data[1],
                gender: data[2],
                birthdayTime: Convert.ToDateTime(data[3]),
                phone: data[4],
                grade: Convert.ToInt32(data[5])));
            }
            fileStream.Close();
            return students;
        }
        /// 求班级同学平均分、最高分、最低分
        /// </summary>
        /// <param name="students"></param>
        public static void OutputImportData(List<Student> students)
        {
            //判断students集合是否为空
            if (!students.Any())
            {
                Console.WriteLine("There is no record.");
                return;
            }
            //求班级同学的平均分
            var average = students.Average(a => a.Grade);
            Console.WriteLine($"班级同学的平均分为：{average}");
            //求最高分同学成绩
            var max = students.Max(a => a.Grade);
            foreach (var item in students)
            {
                if (item.Grade == max)
                {
                    Console.WriteLine($"{item.Name}同学取得最高分：{max}");
                }
            }
            //求最低分同学成绩
            var min = students.Min(a => a.Grade);
            foreach (var item in students)
            {
                if (item.Grade == min)
                {
                    Console.WriteLine($"{item.Name}同学取得最低分：{min}");
                }
            }
        }
    }
}
