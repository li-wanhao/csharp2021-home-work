﻿using System;

namespace Calendar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(DateTime.Now.ToString());//获取当前日期和时间
            Console.WriteLine("———————————");//输出下划线
            Console.Write(DateTime.Now.Year.ToString()+"年");//获取当日所在的年份
            Console.Write(DateTime.Now.Month.ToString() + "月");//获取当日所在的月份
            Console.WriteLine();
            int year = DateTime.Today.Year;//声明一个年的变量
            int month = DateTime.Today.Month;//声明一个月的变量
            int totaldays = DateTime.DaysInMonth(year, month);//声明一个当前月总天数的变量
            int FirstDayOfMonth = Convert.ToInt32(new DateTime(year, month, 1).DayOfWeek-1);//声明一个当前月一号的变量
            int DayOfThisMonth=1,DayOfNextMonth=1;//声明当前月一号和下个月一号变量
            Console.WriteLine(" 一 二 三 四 五 六 日");
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i == 0 && j < FirstDayOfMonth)//输出上个月末
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.Write(Convert.ToString(DateTime.DaysInMonth(year - (12 - month) / 11, (month - 1) + (12 * ((12 - month) / 11))) - FirstDayOfMonth + 1 + j ).PadLeft(3));
                    }
                    else if (DayOfThisMonth  == DateTime.Today.Day)//输出当天
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.Write(Convert.ToString(DayOfThisMonth).PadLeft(3));
                        DayOfThisMonth ++;
                    }
                    else if (DayOfThisMonth <=totaldays && (i>=1||j>=FirstDayOfMonth) )//输出除当天外其他日
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write(Convert.ToString(DayOfThisMonth).PadLeft(3));
                        DayOfThisMonth ++;
                    }
                    else// 输出下个月初
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.Write(Convert.ToString (DayOfNextMonth).PadLeft(3));
                        DayOfNextMonth++;
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
