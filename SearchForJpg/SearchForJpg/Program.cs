﻿using System;
using System.IO;
namespace SearchForJdp
{
    /// <summary>
    /// 使用递归的方法，查找C盘中所有的jpg文件，并计算其数目
    /// </summary>
    class Program
    {
        static string FileAdress = @"c:\";//定义一个字符串并赋值所要操作的目录
        static string FileType = ".jpg";//定义一个字符串并赋值所要查询的jpg文件
        static int count = 0;//定义一个整型变量统计jpg数量
        static void Main(string[] args)
        {
            DirectoryInfo Di = new DirectoryInfo(FileAdress);//指定要操作的目录并赋值给Di
            SearchFor(Di);//调用函数
            Console.WriteLine($"C盘目录中共有{count}个jpg文件");
        }
        static void SearchFor(DirectoryInfo Di)//定义一个函数，使用递归方法，依次查询操作的目录中所以符合条件的文件
        {
            try
            {
                foreach (DirectoryInfo Dir in Di.GetDirectories())//遍历Di目录下所有的子目录，并赋值给Dir
                    SearchFor(Dir);//递归,重复操作该函数
            }
            catch
            {
                return;
            }
            foreach (var Files in Di.GetFiles())//遍历Di目录下所有的文件，并赋值给Files
                if (Files.Extension.ToLower() == FileType)//如果文件的扩展名为jpg则进行
                {
                    count++;
                    Console.WriteLine(Files.FullName);
                }
        }
    }
}